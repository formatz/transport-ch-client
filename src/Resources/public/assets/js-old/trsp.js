/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Winans Creative 2009, Intelligent Spark 2010, iserv.ch GmbH 2010
 * @author     Fred Bliss <fred.bliss@intelligentspark.com>
 * @author     Andreas Schempp <andreas@schempp.ch>
 * @license    http://opensource.org/licenses/lgpl-3.0.html
 */

var HM;

var TransportCH =
{

	/**
	 * Display a "loading data" message
	 * @param string
	 */
	displayBox: function(message, btnClose)
	{
		var box = $('trsp_ajaxBox');
		var overlay = $('trsp_ajaxOverlay');

		if (!overlay)
		{
			overlay = new Element('div').setProperty('id', 'trsp_ajaxOverlay').injectInside($(document.body));
		}

		if (!box)
		{
			box = new Element('div').setProperty('id', 'trsp_ajaxBox').injectInside($(document.body));
		}

		if (btnClose)
		{
			overlay.addEvent('click', TransportCH.hideBox);
			box.addClass('btnClose').addEvent('click', TransportCH.hideBox);
		}

		var scroll = window.getScroll().y;
		//if (Browser.Engine.trident && Browser.Engine.version < 5) { var sel = $$('select'); for (var i=0; i<sel.length; i++) { sel[i].setStyle('visibility', 'hidden'); } }

		overlay.setStyle('display', 'block');
		//overlay.setStyle('top', scroll + 'px');

		box.set('html', message);
		box.setStyle('display', 'block');
		box.setStyle('top', (scroll + 100) + 'px');
	},


	/**
	 * Hide the "loading data" message
	 */
	hideBox: function()
	{
		var box = $('trsp_ajaxBox');
		var overlay = $('trsp_ajaxOverlay');

		if (overlay)
		{
			overlay.setStyle('display', 'none').removeEvents('click');
		}

		if (box)
		{
			box.setStyle('display', 'none').removeEvents('click').removeClass('btnClose');
			if (Browser.Engine.trident && Browser.Engine.version < 5) { var sel = $$('select'); for (var i=0; i<sel.length; i++) { sel[i].setStyle('visibility', 'visible'); } }
		}
	},

	inlineGallery: function(el, elementId)
	{
		$$(('#'+elementId+'_mediumsize img')).set('src', el.href);

		$$(('#'+elementId+'_gallery div, #'+elementId+'_gallery img')).removeClass('active');

		el.addClass('active');
		el.getChildren().addClass('active');

		return false;
	}
};


var TransportCHForm = new Class(
{
	Implements: Options,
	Binds: ['refresh'],
	options: {
		language: 'en',
		action: 'fmd',
		loadMessage: 'Chargement …'
	},

	initialize: function(formId, page, ajaxId, updateId, options)
	{
		this.setOptions(options);

		this.formObj = document.id(formId);
		this.updateDiv = updateId;
		this.ajaxId = ajaxId;

		if (this.formObj)
		{
			this.formObj.set('send',
			{
				url: ('ajax.php?action='+this.options.action+'&page='+page+'&id='+ajaxId+'&language='+this.options.language),
				link: 'cancel',
				evalScripts: false,
				onRequest: function()
				{
					TransportCH.displayBox(this.options.loadMessage);
				}.bind(this),
				onSuccess: function(txt, xml)
				{
                    /*data = JSON.decode(txt);*/
					document.id(this.updateDiv).innerHTML = txt;
					TransportCH.hideBox();
					
					window.fireEvent('ajaxready');
				}.bind(this),
				onFailure: function()
				{
					TransportCH.hideBox();
				}
			});
			
			this.formObj.addEvent('submit', function(){
				HM.remove('link');
				HM.set('search',document.id('ctrl_keywords_'+this.ajaxId).value);
			}.bind(this));
			
			this.formObj.addEvent('submit2', function(){
				this.formObj.send();
			}.bind(this));
		}
	},

	refresh: function(event)
	{
		this.formObj.send();
	}
});

var TransportCHLink = new Class(
{
	Implements: Options,
	Binds: ['refresh'],
	options: {
		language: 'en',
		action: 'fmd',
		loadMessage: 'Chargement …'
	},

	initialize: function(linkId, page, ajaxId, updateId, type, value, options)
	{
		this.setOptions(options);

		this.link = document.id(linkId);

		if (this.link)
		{
			this.link.addEvent('click', function(e){
				HM.remove('search');
				HM.set('link',linkId);
                e.preventDefault();
			}.bind(this));
			
			this.link.addEvent('click2', function(e){
				new Request.HTML({
					url: ('ajax.php?action='+this.options.action+'&id='+ajaxId+'&page='+page+'&'+type+'='+value+'&language='+this.options.language),
					method: 'get',
                    //evalScripts: true,
                    update:updateId,
					onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript)
					{
                        //response = JSON.decode(responseText);
                        //document.id(updateId).innerHTML = response.content;
                        //document.id(updateId).innerHTML = responseText;
                        $exec(responseJavaScript);
					},
					onComplete: function()
					{
						//console.log('ajax complete!');
						TransportCH.hideBox();
                        window.fireEvent('ajaxready');
					},
					onRequest: function()
					{
						TransportCH.displayBox(this.options.loadMessage);
					}.bind(this),
					onFailure: function()
					{
						TransportCH.hideBox();
					}
				}).send();
				
			}.bind(this));
		}
	}
});

window.addEvent('domready',function(){
	
	HM = new HistoryManager({delimiter:'!'});

	HM.addEvent('link:added',function(new_value){
		HM.remove('search');
		document.id(new_value).fireEvent('click2');
		//console.log('link was added:'+new_value);
	});
	HM.addEvent('link:updated',function(new_value){
		document.id(new_value).fireEvent('click2');
		//console.log('link was modified:'+new_value);
	});
	HM.addEvent('search:added',function(new_value){
		HM.remove('link');
		document.id('ctrl_keywords_90').value = new_value;
		document.id('stand_search_90').fireEvent('submit2');
		//console.log('search was added:'+new_value);
	});
	HM.addEvent('search:updated',function(new_value){
		document.id('ctrl_keywords_90').value = new_value;
		document.id('stand_search_90').fireEvent('submit2');
		//console.log('link was modified:'+new_value);
	});
	HM.start();
});