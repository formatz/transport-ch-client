<?php

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['expo_stands']    = '{title_legend},name,headline,type;{config_legend},domainUrl,client_id,client_secret,apiUsername,apiPassword;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['domainUrl'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['domainUrl'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'sql'                     => "varchar(255) NULL"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['client_id'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['client_id'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'sql'                     => "varchar(255) NULL"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['client_secret'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['client_secret'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'sql'                     => "varchar(255) NULL"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['apiUsername'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['apiUsername'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'sql'                     => "varchar(255) NULL"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['apiPassword'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['apiPassword'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'sql'                     => "varchar(255) NULL"
);
