<?php
/**
 * Created by PhpStorm.
 * User: fabien
 * Date: 18.09.15
 * Time: 15:43
 */

namespace Contao;

use Formatz\ExpomanagerClientBundle\ExpoManager\ApiClient;
use Formatz\ExpomanagerClientBundle\ExpoManager\OAuth2GrantFlow;

class ModuleStands extends \Module
{
    const YEAR = 2019;
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_stands';

    protected $strFormId = 'stand_search_';
    protected $categoryLinkId = 'stand_search_category_';
    protected $brandLinkId = 'stand_search_brand_';
    protected $standLinkId = 'stand_search_stand_';
    protected $expLinkId = 'stand_search_exp_';

    protected $updateDivId = 'stand_search_result';

    /** @var ApiClient */
    protected $expomanagerApiClient;

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### EXPO-MANAGER Stands ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }
        else {
            $apiAuth = $this->getContainer()->get(OAuth2GrantFlow::class);
            $apiAuth
                ->setClientId($this->client_id)
                ->setClientSecret($this->client_secret)
                ->setDomainUrl($this->domainUrl)
            ;
            if(!empty($this->apiUsername) && !empty($this->apiPassword)) {
                $apiAuth
                    ->setUsername($this->apiUsername)
                    ->setPassword($this->apiPassword)
                ;
            }
            $this->expomanagerApiClient = $this->getContainer()->get(ApiClient::class);
        }

        return parent::generate();
    }

    public function generateAjax()
    {
        $this->loadLanguageFile('modules');
        $this->loadLanguageFile('tl_exp_stand');

        if($this->Input->get('category') == 'all'){
            return $this->getCategories();
        }
        if($this->Input->get('stand') == 'all'){
            return $this->getStandsList();
        }
        if($this->Input->get('brand') == 'all'){
            return $this->getBrands();
        }
        if($this->Input->get('exp') == 'all'){
            return $this->getExps();
        }

        $strKeywords = trim($this->Input->get('keywords'));
        $strKeywords = preg_replace('/\{\{[^\}]*\}\}/', '', $strKeywords);
        if($strKeywords) {
            return $this->search($strKeywords);
        }


        if($this->Input->get('category')){
            $cat = $this->Input->get('category');
            if(is_numeric($cat))
                return $this->getStandsPerCategory($cat);
        }
        if($this->Input->get('stand')){
            $stand = $this->Input->get('stand');
            if(!is_numeric($stand) && strpos($stand, '_') !== false) {
                System::log('loading non-numeric stand : ' . $stand, 'ModuleStands ' . __METHOD__, TL_ERROR);
                list($stand) = explode('_', $stand, -1);
                System::log('stand readed : ' . $stand, 'ModuleStands ' . __METHOD__, TL_ERROR);
            }
            if(is_numeric($stand)) {
                return $this->getStand($stand);
            }
        }
        if($this->Input->get('brand')){
            $brand = $this->Input->get('brand');
            if(is_numeric($brand))
                return $this->getStandsPerBrand($brand);
        }
    }


    /**
     * Compile the current element
     */
    protected function compile()
    {
        global $objPage;

        //form
        // Remove insert tags
        $strKeywords = trim($this->Input->get('keywords'));
        $strKeywords = preg_replace('/\{\{[^\}]*\}\}/', '', $strKeywords);

        $objFormTemplate = new FrontendTemplate('mod_stand_search_form');
        $objFormTemplate->formId = $this->strFormId.$this->id;
        $objFormTemplate->keyword = specialchars($strKeywords);
        $objFormTemplate->keywordLabel = "search label";
        $objFormTemplate->uniqueId = $this->id;
        $objFormTemplate->search = specialchars($GLOBALS['TL_LANG']['MSC']['searchLabel']);
        $objFormTemplate->id = ($GLOBALS['TL_CONFIG']['disableAlias'] && $this->Input->get('id')) ? $this->Input->get('id') : false;
        $objFormTemplate->action = $this->generateFrontendUrl($objPage->row());

        $this->Template->form = $objFormTemplate->parse();

        $this->loadLanguageFile('modules');
        $this->loadLanguageFile('tl_exp_stand');

        $objFormTemplate = new FrontendTemplate('mod_stand_search_links');
        $objFormTemplate->standId = $this->standLinkId.$this->id;
        $objFormTemplate->standLabel = $GLOBALS['TL_LANG']['MOD']['stand'][0];
        $objFormTemplate->standLink = $this->generateFrontendUrl($objPage->row(), '/stand/all');
        $objFormTemplate->categoryId = $this->categoryLinkId.$this->id;
        $objFormTemplate->categoryLabel = $GLOBALS['TL_LANG']['MOD']['productCategories'][0];
        $objFormTemplate->categoryLink = $this->generateFrontendUrl($objPage->row(), '/product-category/all');
        $objFormTemplate->brandId = $this->brandLinkId.$this->id;
        $objFormTemplate->brandLabel = $GLOBALS['TL_LANG']['MOD']['brands'][0];
        $objFormTemplate->brandLink = $this->generateFrontendUrl($objPage->row(), '/brand/all');
        $objFormTemplate->expId = $this->expLinkId.$this->id;
        $objFormTemplate->expLabel = $GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'];
        $objFormTemplate->expLink = $this->generateFrontendUrl($objPage->row(), '/exp/all');

        $this->Template->links = $objFormTemplate->parse();

        $this->Template->updateDivId = $this->updateDivId.$this->id;

        $assetsDir = 'bundles/formatzexpomanagerclient/assets/';
        $suffix = ''; // '.min'
        $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . 'js/jquery-scrollto' . $suffix . '.js';
        $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . 'js/jquery.history' . $suffix . '.js';
        $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . 'js/expomanager' . $suffix . '.js';
        $GLOBALS['TL_CSS'][] = $assetsDir . 'css/trsp.min.css';

        if($this->Input->get('category') == 'all'){
            $this->Template->search_content = $this->getCategories();
        }
        elseif($this->Input->get('product-category') == 'all'){
            $this->Template->search_content = $this->getProductCategories();
        }
        elseif($this->Input->get('stand') == 'all'){
            $this->Template->search_content = $this->getStandsList();
        }
        elseif($this->Input->get('brand') == 'all'){
            $this->Template->search_content = $this->getBrands();
        }
        elseif($this->Input->get('exp') == 'all'){
            $this->Template->search_content = $this->getExps();
        }
        elseif($strKeywords){
            $this->Template->search_content = $this->search($strKeywords);
        }
        elseif($this->Input->get('category')){
            $cat = $this->Input->get('category');
            $this->Template->search_content = $this->getStandsPerCategory($cat);
        }
        elseif($this->Input->get('product-category')){
            $cat = $this->Input->get('product-category');
            $this->Template->search_content = $this->getExhibitorsPerProductCategory($cat);
        }
        elseif($this->Input->get('brand')){
            $brand = $this->Input->get('brand');
            $this->Template->search_content = $this->getStandsPerBrand($brand);
        }
        elseif($this->Input->get('stand')){
            $stand = $this->Input->get('stand');
            $second = false;
            if(!is_numeric($stand) && strpos($stand, '_') !== false) {
                list($stand) = explode('_', $stand, -1);
                $second = true;
            }
            $this->Template->search_content = $this->getStand($stand, $second);
        }
    }

    private function renderProductCategoryTree($category, int $level)
    {
        global $objPage;
        $children = [];
        $categoryChildren = $category->children;
        if($level >= 1 && !empty($categoryChildren)) {
            usort($categoryChildren, function($a, $b) {
                return strcmp($a->labels->{$GLOBALS['TL_LANGUAGE']}->label, $b->labels->{$GLOBALS['TL_LANGUAGE']}->label);
            });
        }
        foreach($categoryChildren as $child) {
            $children[] = $this->renderProductCategoryTree($child , $level + 1);
        }
        $label = $category->labels->{$GLOBALS['TL_LANGUAGE']}->label;
        $params = [
            'level' => $level,
            'class' => 'lvl_' . $level,
            'content' => $this->generateLink(
                "cat_all_".$category->id,
                $this->generateFrontendUrl($objPage->row(), '/product-category/'.$category->id),
                $label,
                $label),
            'children' => $children,
        ];

        $objTemplate = new FrontendTemplate('standsearch_tree_item');
        $objTemplate->setData($params);
        return $objTemplate->parse();
    }

    private function getProductCategories()
    {
        global $objPage;
        $arrItems = [];
        $categories = $this->expomanagerApiClient->getProductCategories();

        foreach($categories as $category) {
            $arrItems[] = [
                'content' => $this->renderProductCategoryTree($category, 1),
                'class' => '',
            ];
        }

        $objTemplate = new FrontendTemplate('standsearch_tree');
        $objTemplate->setData([
            'title' => $GLOBALS['TL_LANG']['MOD']['productCategories'][0],
            'items' => $arrItems,
            'class' => '',
        ]);
        return $objTemplate->parse();
    }

    protected function getExhibitorsPerProductCategory(string $categorySearched)
    {
        $exhibitors = $this->expomanagerApiClient->getExhibitors();
        $exhibitors = array_filter($exhibitors, function($exhibitor) use ($categorySearched) {
            foreach($exhibitor->product_categories as $productCategory) {
                if($productCategory->id == $categorySearched){
                    return true;
                }
            }
            return false;
        });

        //order by company
        usort($exhibitors, function($a, $b) {
            return strcmp(mb_strtolower($a->company), mb_strtolower($b->company));
        });

        $arrLinks = array();
        foreach($exhibitors as $exhibitor) {
            $arrLinks = array_merge($arrLinks, $this->renderExhibitorLinks($exhibitor));
        }

        $objTemplate = new FrontendTemplate('standsearch_list');
        $objTemplate->title = $GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'];
        $objTemplate->items = $arrLinks;
        return $objTemplate->parse();
    }

    private function getCategories()
    {
        global $objPage;
        $displayField = $GLOBALS['TL_LANGUAGE'] == 'de' ? 'nameDe':'nameFr';
        $arrLinks= array();
        $time = time();
        $categories = $this->expomanagerApiClient->getCategories();
        foreach($categories as $category) {
            if(isset($GLOBALS['TL_LANG']['tl_exp_stand']['category'][$category->letter])) {
                $label = $GLOBALS['TL_LANG']['tl_exp_stand']['category'][$category->letter];
            }
            else {
                $label = $category->$displayField;
            }
            $arrLinks[] = array('content' => $this->generateLink(
                "cat_all_".$category->id."_".$time,
                $this->generateFrontendUrl($objPage->row(), '/category/'.$category->id),
                $category->letter . ') ' . $label,
                $category->$displayField)
            );
        }

        $objTemplate = new FrontendTemplate('standsearch_list');
        $objTemplate->title = $GLOBALS['TL_LANG']['MOD']['categories'][0];
        $objTemplate->items = $arrLinks;
        return $objTemplate->parse();
    }

    protected function getStandsPerCategory($categorySearched)
    {
        $stands = $this->expomanagerApiClient->getStands();
        $ids = $idsSecond = array();
        foreach($stands as $stand) {
            foreach($stand->categories as $category) {
                if($category->id == $categorySearched){
                    $ids[] = $stand->id;
                    break;
                }
            }
            foreach($stand->categories_second as $category) {
                if($category->id == $categorySearched){
                    $idsSecond[] = $stand->id;
                    break;
                }
            }
        }
        return $this->getStandsList($ids, $idsSecond);
    }

    protected function getBrands()
    {
        global $objPage;
        $displayField = 'name';

        $brands = $this->expomanagerApiClient->getBrands();

        $arrLinks = array();
        //$strAjax = '';
        $time = time();
        foreach($brands as $brand) {
            $arrLinks[] = array('content' => $this->generateLink(
                "brand_".$brand->id."_".$time,
                $this->generateFrontendUrl($objPage->row(), '/brand/'.$brand->id),
                $brand->$displayField,
                $brand->$displayField
            ));
            //$strAjax .= $this->generateAjaxLink("brand_".$objBrand->id."_".$time, $objPage->id, 'brand', $objBrand->id);
        }
        /*$strAjax = '<script>
window.addEvent(\'ajaxready\', function() {
' . $strAjax . '
});
</script>';*/

        $objTemplate = new FrontendTemplate('standsearch_list');
        $objTemplate->title = $GLOBALS['TL_LANG']['MOD']['brands'][0];
        $objTemplate->items = $arrLinks;
        //$objTemplate->ajax = $strAjax;
        //$this->log($strAjax, "ModuleStandSearch getCategories()", TL_GENERAL);
        return $objTemplate->parse();
    }

    private function getStandsPerBrand($brandSearched)
    {
        $stands = $this->expomanagerApiClient->getStands();
        $ids = $idsSecond = array();
        foreach($stands as $stand) {
            foreach($stand->brands as $brand) {
                if($brand->id == $brandSearched){
                    $ids[] = $stand->id;
                    break;
                }
            }
            foreach($stand->brands_second as $brand) {
                if($brand->id == $brandSearched){
                    $idsSecond[] = $stand->id;
                    break;
                }
            }
        }
        return $this->getStandsList($ids, $idsSecond);
    }

    private function getStandsList($arrIds = null, $arrIdsSecond = null)
    {
        global $objPage;

        $stands = $this->expomanagerApiClient->getStands();

        $stands = array_filter($stands, function($stand) use($arrIds, $arrIdsSecond) {
            if($arrIds === null || in_array($stand->id, $arrIds)) {
                return true;
            }
            if($arrIdsSecond !== null && in_array($stand->id, $arrIdsSecond)) {
                return true;
            }
            return false;
        });

        if($arrIds !== null && $arrIdsSecond !== null) {
            $stands = array_map(function ($stand) use ($arrIds, $arrIdsSecond) {
                //le stand est affiché que pour le second exposant
                if (!in_array($stand->id, $arrIds) && in_array($stand->id, $arrIdsSecond)) {
                    $stand->name_main = $stand->name_second;
                }
                return $stand;
            }, $stands);

            foreach ($stands as $stand) {
                //le stand est affiché 2x
                if (in_array($stand->id, $arrIds) && in_array($stand->id, $arrIdsSecond)) {
                    $second = clone $stand;
                    $second->logo = null;
                    $second->name_main = $stand->name_second;
                    $stands[] = $second;
                }
            }
        }
        else {
            foreach ($stands as $stand) {
                //le stand est affiché 2x
                if ($stand->name_second) {
                    $second = clone $stand;
                    $second->logo = null;
                    $second->name_main = $stand->name_second;
                    $stands[] = $second;
                }
            }
        }

        usort($stands, function($a, $b) {
            return strcmp($a->name_main, $b->name_main);
        });

        $arrLinks = array();
        foreach($stands as $stand) {
            $arrLinks[] = $this->renderStandLink($stand, $objPage, ($stand->name_second == $stand->name_main));
        }

        $objTemplate = new FrontendTemplate('standsearch_list');
        $objTemplate->class = 'stand_list';
        $objTemplate->title = $GLOBALS['TL_LANG']['MOD']['stand'][0];
        $objTemplate->items = $arrLinks;
        //$objTemplate->ajax = $strAjax;
        return $objTemplate->parse();
    }

    private function getStand($id, $second = false)
    {
        if(!is_numeric($id))
            return '';

        return $this->renderStand($id, null, $second);
    }

    protected function renderStandLink($stand, $objPage, $second = false)
    {
        $displayField = 'name_main';
        $class = '';
        $strLink = $this->generateLink(
            "stand_".$stand->id . ($second ? '_s' : ''),
            $this->generateFrontendUrl($objPage->row(), '/stand/'.$stand->id . ($second ? '_s' : '')),
            $stand->$displayField . " (" . $stand->hall . ', ' . $stand->number . ')',
            $stand->$displayField
        );
        if(!$second) {
            $logo = $stand->logo;
        }
        else {
            $logo = $stand->logoSecond;
        }
        if($logo) {
            $strImage = '<img src="' . $this->domainUrl . \System::urlEncode($logo) . '" alt="' . specialchars('logo ' . $stand->$displayField) . '"' . ' class="stand_logo"' . '>';

            //$strImage = \Image::getHtml($this->domainUrl . $stand->logo, 'logo ' . $stand->$displayField, 'class="stand_logo"');
            $strLink = $strLink . $strImage;
            $class = 'link_logo';
        }
        return array(
            'class' => $class,
            'content' => $strLink
        );
    }

    protected function renderExhibitorLinks($exhibitor)
    {
        global $objPage;
        $standSelected = array();
        foreach($exhibitor->stands_main as $stand) {
            if($stand->year == self::YEAR) {
                $standSelected[] = $stand;
            }
        }
        foreach($exhibitor->stands_co as $stand) {
            if($stand->year == self::YEAR) {
                $standSelected[] = $stand;
            }
        }

        $links = array();
        foreach($standSelected as $stand) {
            $links[] = array('content' => $this->generateLink(
                "stand_".$stand->id."_".substr(md5($exhibitor->company),0,8),
                $this->generateFrontendUrl($objPage->row(), '/stand/'.$stand->id),
                $exhibitor->company . ' (' . $stand->hall . ', ' . $stand->number . ')',
                $exhibitor->company
            ));
        }


        $standSelected = array();
        foreach($exhibitor->stands_second as $stand) {
            if($stand->year == self::YEAR) {
                $standSelected[] = $stand;
            }
        }
        foreach($standSelected as $stand) {
            $links[] = array('content' => $this->generateLink(
                "stand_".$stand->id."_s_".substr(md5($exhibitor->company),0,8),
                $this->generateFrontendUrl($objPage->row(), '/stand/'.$stand->id . '_s'),
                $exhibitor->company . ' (' . $stand->hall . ', ' . $stand->number . ')',
                $exhibitor->company
            ));
        }

        return $links;
    }

    protected function renderStand($standId, $stand = null, $second = false)
    {
        if($stand === null) {
            $stand = $this->expomanagerApiClient->getStand($standId);
        }
        if(empty($stand)) {
            return '';
        }
        if($second && (empty($stand->name_second) || !$stand->exhibitor_second)) {
            //contrôle si le second exposant existe bien, sinon ne pas tenir compte de ça
            $second = false;
        }

        $this->loadLanguageFile('tl_exp_stand');
        $this->loadDataContainer('tl_exp_stand');
        //fields : numéro, halle, categories, marques
        $standInfos = array();
        $standInfos[] = array('label' => $GLOBALS['TL_LANG']['tl_exp_stand']['number'][0], 'value' => $stand->number);
        $standInfos[] = array('label' => $GLOBALS['TL_LANG']['tl_exp_stand']['hall'][0], 'value' => $stand->hall);
        if(!empty($stand->categories) || !empty($stand->categories_second)) {
            $displayField = $GLOBALS['TL_LANGUAGE'] == 'fr' ? 'name_fr':'name_de';
            $values = array();
            $categories = $second ? $stand->categories_second : $stand->categories;
            foreach($categories as $category) {
                if(isset($GLOBALS['TL_LANG']['tl_exp_stand']['category'][$category->letter])) {
                    $label = $GLOBALS['TL_LANG']['tl_exp_stand']['category'][$category->letter];
                }
                else {
                    $label = $category->$displayField;
                }
                $values[] = $category->letter . ') ' . $label;
            }
            $standInfos[] = array('label' => $GLOBALS['TL_LANG']['tl_exp_stand']['categories'][0], 'value' => implode("<br />",$values));
        }
        if(!empty($stand->brands)) {
            $values = array();
            $brands = $second ? $stand->brands_second : $stand->brands;
            foreach($brands as $brand) {
                $values[] = $brand->name;
            }
            $standInfos[] = array('label' => $GLOBALS['TL_LANG']['tl_exp_stand']['brands'][0], 'value' => implode("<br />",$values));
        }
        //companies
        $arrCompanies = array();

        //Exposant ou Second Exposant
        if($second) {
            $arrCompanies[] = $this->renderExhibitor($stand->exhibitor_second);
        }
        else {
            $arrCompanies[] = $this->renderExhibitor($stand->exhibitor);
        }

        $objTemplate = new FrontendTemplate('standsearch_reader');

        $objTemplate->standLabel = "Stand";
        $objTemplate->name = ($second ? $stand->name_second : $stand->name_main);
        //$objTemplate->stand_raw = $stand;
        $objTemplate->infos = $standInfos;
        $objTemplate->companies = implode("",$arrCompanies);
        $objTemplate->companiesLabel = $GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'];
        if(!$second) {
            $logo = $stand->logo;
        }
        else {
            $logo = $stand->logoSecond;
        }
        $objTemplate->logo = ($logo ? '<img src="' . $this->domainUrl . \System::urlEncode($logo) . '" alt="' . specialchars('logo ' . $objTemplate->name) . '"' . ' class="stand_logo"' . '>' : '');

        //CO-EXPOSANTS
        if(!$second && !empty($stand->co_exhibitors)) {
            $strCompanies2 = '';
            foreach($stand->co_exhibitors as $coExhibitor) {
                $strCompanies2 .= $this->renderExhibitor($coExhibitor);
            }
            $objTemplate->companies2Label = $GLOBALS['TL_LANG']['tl_exp_stand']['co-exhibitor'];
            $objTemplate->companies2 = $strCompanies2;
        }

        return $objTemplate->parse();
    }

    protected function renderExhibitor($exhibitor, $title = null)
    {
        $objTemplateCompany = new FrontendTemplate('standsearch_reader_company');
        if($title !== null) {
            $objTemplateCompany->title = $title;
        }
        else {
            $objTemplateCompany->title = $exhibitor->company;
            if ($exhibitor->company2) {
                $objTemplateCompany->title .= "<br />" . $exhibitor->company2;
            }
        }
        $arrCompanyInfos = array();
        //fields : street(+postal +city +country) phone fax email website
        $street = ($exhibitor->street ? $exhibitor->street . ($exhibitor->street2 ? '<br />'.$exhibitor->street2 : '') : '');
        $street .= $exhibitor->postalCase ? '<br />'.$exhibitor->postalCase.'' : '';
        $street .= '<br />' . $exhibitor->postal . ' '. $exhibitor->city;
        if($exhibitor->country !== 'CH' && isset($exhibitor->country_labels->{$GLOBALS['TL_LANGUAGE']})) {
            $street .= '<br />' . $exhibitor->country_labels->{$GLOBALS['TL_LANGUAGE']};
        }
        $arrCompanyInfos[] = array('label' => $GLOBALS['TL_LANG']['tl_exp_stand']['street'][0], 'value' => $street);
        $displayedFields = array('phone', 'fax','email','website', 'website2');
        foreach($displayedFields as $displayedField){
            $label = $GLOBALS['TL_LANG']['tl_exp_stand'][$displayedField][0];
            $value = $exhibitor->$displayedField;

            if($value == '')
                continue;

            if($displayedField == 'website' || $displayedField == 'website2') {
                $href = strpos($value, 'http://') !== 0 ? 'http://'.$value : $value;
                $value = sprintf('<a target"_blank" href="%s">%s</a>', $href, $value);
            }
            else if($displayedField == 'email') {
                $value = '{{email::'.$value.'}}';
            }

            $arrCompanyInfos[] = array('label' => $label, 'value' => $value);
        }

        $objTemplateCompany->infos = $arrCompanyInfos;

        return $objTemplateCompany->parse();
    }

    protected function getExps()
    {
        global $objPage;

        $exhibitors = $this->expomanagerApiClient->getExhibitors(true);

        //order by company
        usort($exhibitors, function($a, $b) {
            return strcmp(mb_strtolower($a->company), mb_strtolower($b->company));
        });

        $arrLinks = array();
        foreach($exhibitors as $exhibitor) {
            $arrLinks = array_merge($arrLinks, $this->renderExhibitorLinks($exhibitor, $objPage));
        }

        $objTemplate = new FrontendTemplate('standsearch_list');
        $objTemplate->title = $GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'];
        $objTemplate->items = $arrLinks;
        return $objTemplate->parse();
    }


    protected function search($strKeywords)
    {
        global $objPage;
        // Execute the search if there are keywords
        if ($strKeywords == '' || $strKeywords == '*')
            return '';

        $results = $this->expomanagerApiClient->search($strKeywords);
        $results->keywords = $strKeywords;

        //print results
        $objTemplate = new FrontendTemplate('standsearch_results');
        //$strAjax = '';
        //list stands
        if(!empty($results->stands)) {
            $stands = $results->stands;
            foreach($stands as $stand) {
                if($stand->name_second) {
                    $second = clone $stand;
                    $second->logo = null;
                    $second->name_main = $stand->name_second;
                    $stands[] = $second;
                }
            }

            usort($stands, function($a, $b) {
                return strcmp($a->name_main, $b->name_main);
            });

            $arrLinks = array();
            foreach($stands as $stand) {
                $arrLinks[] = $this->renderStandLink($stand, $objPage, ($stand->name_second == $stand->name_main));
            }

            $objTemplateList = new FrontendTemplate('standsearch_list');
            $objTemplateList->title = $GLOBALS['TL_LANG']['MOD']['stand'][0];
            $objTemplateList->items = $arrLinks;

            $objTemplate->stands = $objTemplateList->parse();
        }
        if(!empty($results->exhibitors)) {
            $arrLinks = array();
            foreach($results->exhibitors as $exhibitor){
                $arrLinks = array_merge($arrLinks, $this->renderExhibitorLinks($exhibitor, $objPage));
            }

            $objTemplateList = new FrontendTemplate('standsearch_list');
            $objTemplateList->title = $GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'];
            $objTemplateList->items = $arrLinks;

            $objTemplate->exps = $objTemplateList->parse();
        }
        if(count($results->brands)){
            $arrLinks = array();
            foreach($results->brands as $brand){
                $arrLinks[] = array('content' => $this->generateLink(
                    "brand_".$brand->id,
                    $this->generateFrontendUrl($objPage->row(), '/brand/'.$brand->id),
                    $brand->name,
                    $brand->name
                ));
            }

            $objTemplateList = new FrontendTemplate('standsearch_list');
            $objTemplateList->title = $GLOBALS['TL_LANG']['MOD']['brands'][0];
            $objTemplateList->items = $arrLinks;

            $objTemplate->brands = $objTemplateList->parse();
        }
        return $objTemplate->parse();
    }

    private function generateLink($linkId, $href, $label, $title)
    {
        return sprintf('<a id="%s" href="%s" title="%s">%s</a>',$linkId, $href, $title, $label);
    }
}
