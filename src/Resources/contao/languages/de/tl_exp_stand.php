<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_exp_stand'][''] = array('', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['exhib_name'] = array('Standname', 'Name der Firma für die Messe, wie er im Messekatalog erscheinen soll.');

$GLOBALS['TL_LANG']['tl_exp_stand']['name'] = array('Interner Name', 'Erfassung des internen Standnamens (provisorisch)');
$GLOBALS['TL_LANG']['tl_exp_stand']['alias'] = array('Alias', 'Einzigartiger Alias, der in den URLs anstelle des Benutzers verwendet wird.');
$GLOBALS['TL_LANG']['tl_exp_stand']['company'] = array('Firma', 'Hier können Sie einen Firmennamen eingeben.');
$GLOBALS['TL_LANG']['tl_exp_stand']['company2'] = array('Firma (Zusatz)', 'Firmenname (Zusatz)');
$GLOBALS['TL_LANG']['tl_exp_stand']['street'] = array('Adresse', 'Bitte geben Sie den Strassennamen und die Hausnummer ein.');
$GLOBALS['TL_LANG']['tl_exp_stand']['street2'] = array('Adresse (Zusatz)', 'Saisir le nom et le numéro de la rue.');
$GLOBALS['TL_LANG']['tl_exp_stand']['postalCase'] = array('Postfach', 'Postfach');
$GLOBALS['TL_LANG']['tl_exp_stand']['postal'] = array('Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.');
$GLOBALS['TL_LANG']['tl_exp_stand']['city'] = array('Stadt', 'Bitte geben Sie den Namen der Stadt ein.');
$GLOBALS['TL_LANG']['tl_exp_stand']['country'] = array('Land', 'Wählen Sie das Land.');
$GLOBALS['TL_LANG']['tl_exp_stand']['phone'] = array('Telefonnummer (Firma)', 'Bitte geben Sie die Telefonnummer ein.');//('Telefonnummer (Firma)', 'Bitte geben Sie die Telefonnummer ein.');
$GLOBALS['TL_LANG']['tl_exp_stand']['fax'] = array('Faxnummer', 'Bitte geben Sie die Faxnummer ein.');
$GLOBALS['TL_LANG']['tl_exp_stand']['email'] = array('E-Mail-Adresse', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
$GLOBALS['TL_LANG']['tl_exp_stand']['website'] = array('Webseite', 'Hier können Sie eine Internet-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_exp_stand']['website2'] = array('Webseite 2', 'Hier können Sie eine Internet-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_exp_stand']['language'] = array('Sprache', 'Bitte wählen Sie die Sprache.');
$GLOBALS['TL_LANG']['tl_exp_stand']['categories'] = array('Standkategorien', 'N\'indiquez qu\'une catégorie - la catégorie principale');
$GLOBALS['TL_LANG']['tl_exp_stand']['brands'] = array('Markenvertretungen', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_opt'] = array('Optimale Dimensionen', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_min'] = array('Minimale Dimensionen', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_max'] = array('Maximale Dimensionen', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_internet'] = array('Internet drahtlos', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_electric'] = array('Elektroanschluss', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_water'] = array('Wasseranschluss', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['remark'] = array('Bemerkungen', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['associations'] = array('Verbände', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts'] = array('Fronten', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['final_registered'] = array('Definitive Anmeldung','Definitive Anmeldung');
$GLOBALS['TL_LANG']['tl_exp_stand']['number'] = array('Nummer', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['hall'] = array('Halle', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars'] = array('Wir stellen aus :','Ausstellungsgüter');
$GLOBALS['TL_LANG']['tl_exp_stand']['files'] = array('Fichiers joints', 'Vous avez la possibilité de joindre des fichiers à votre dossier personnel.');


/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_instDetails'] = 'Technische Einrichtungen';
$GLOBALS['TL_LANG']['tl_exp_stand']['enterprise_header'] = 'Firma';
$GLOBALS['TL_LANG']['tl_exp_stand']['co-exhibitor'] = 'Mitaussteller';
$GLOBALS['TL_LANG']['tl_exp_stand']['exhibCat'] = "Eintrag Messekatalog";
$GLOBALS['TL_LANG']['tl_exp_stand']['hasSecondExp'] = "Zweitaussteller (s. Allgemeines Reglement Art. 3.4.).";
$GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'] = "Aussteller";
$GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails2'] = "Zweitaussteller";
$GLOBALS['TL_LANG']['tl_exp_stand']['categoriesDetails'] = "Standkategorie";
$GLOBALS['TL_LANG']['tl_exp_stand']['brandsDetails'] = "Offizielle Markenvertretungen";
$GLOBALS['TL_LANG']['tl_exp_stand']['standDetails'] = "Informationen zum Stand";
$GLOBALS['TL_LANG']['tl_exp_stand']['associationsDetails'] = "Verbandsmitgliedschaften";
$GLOBALS['TL_LANG']['tl_exp_stand']['confirmHeader'] = "Bestätigung";
$GLOBALS['TL_LANG']['tl_exp_stand']['textConfirm'] = 'Mit dem Abhaken dieses Kästchens bestätigt der/die oben eingeschriebene Standverantwortliche(r), dass er/sie <a href="tl_files/transport/Conditions_part/tCH13_Conditions_de_participation_121009D_BAT_lt.pdf" target="_blank">die Teilnahmebedingungen</a> der entsprechenden Ausgabe des Schweizer Nutzfahrzeugsalons transportCH gelesen und für gut befunden hat.';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableTitle'] = "Dimensionen";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesOptTitle'] = "Optimal [m]";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesMinTitle'] = "Minimal [m]";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesMaxTitle'] = "Maximal [m]";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableL'] = "Länge";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableH'] = "Höhe";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableP'] = "Tiefe";
$GLOBALS['TL_LANG']['tl_exp_stand']['outSizesDetails'] = "Freigelände";
$GLOBALS['TL_LANG']['tl_exp_stand']['respStandHeader'] = "Standverantwortliche(r)";
$GLOBALS['TL_LANG']['tl_exp_stand']['billingStandHeader'] = "Rechnungsveranwortliche(r)";
$GLOBALS['TL_LANG']['tl_exp_stand']['voirDelaisEtTarifs'] = "";
$GLOBALS['TL_LANG']['tl_exp_stand']['brandsExpl'] = 'Erfassen Sie de Marke. Wählen Sie eine aus der Liste oder fügen Sie eine neue ein und bestätigen Sie mit ENTER.';

$GLOBALS['TL_LANG']['tl_exp_stand']['files_droplabel'] = 'Déposez les fichiers désirés à cet endroit ...';

$GLOBALS['TL_LANG']['tl_exp_stand']['date_registered'] = array('Anmeldungsdatum','');

$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['both'] = 'Schwere und leichte Nutzfahrzeuge';
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['light'] = 'Leichte Nutzfahrzeuge';
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['heavy'] = 'Schwere Nutzfahrzeuge';
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['no'] = 'Keine Fahrzeuge';

$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['0'] = 'Kein';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['1'] = '1 Front';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['2a'] = '2 Fronten (Ecke)';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['2b'] = '2 Fronten (vis-a-vis)';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['3'] = '3 Fronten';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['4'] = '4 Fronten';

$GLOBALS['TL_LANG']['tl_exp_stand']['total_registered_stands'] = 'Per heute sind wurden bereits %s Stände angemeldet.';


/* expo-manager */
$GLOBALS['TL_LANG']['tl_exp_stand']['category']['a'] = 'Schwere Nutzfahrzeuge';
$GLOBALS['TL_LANG']['tl_exp_stand']['category']['b'] = 'Leichte Nutzfahrzeuge';

?>
