<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_member']['hasEnterprise'] = array('Gehört einer Firma an.','');
$GLOBALS['TL_LANG']['tl_member']['enterprises'] = array('Firmen', 'Wählen Sie die Firmen des Benutzers und weisen Sie ihm eine Funktion zu.');
$GLOBALS['TL_LANG']['tl_member']['generateAccount'] = array('Konto generieren', 'Haken Sie das Kästchen ab, um einen Benutzernamen und ein Passwort zu generieren, dass dem Benutzer automatisch gesandt wird (Überprüfen Sie die Email-Adresse).');
$GLOBALS['TL_LANG']['tl_member']['access_type'] = array('Zugangsart', 'Handelt es sich um eine Person (Werkswinstellung) oder nur um einen Zugang zu einem Stand (Login / Passwort).');
$GLOBALS['TL_LANG']['tl_member']['stand'] = array('Stand', 'Wählen Sie den Stand in Bezug mit diesem Zugang.');
$GLOBALS['TL_LANG']['tl_member']['postalCase'] = array('Postfach', 'Postfach');
$GLOBALS['TL_LANG']['tl_member']['street'] = array('Adresse');
$GLOBALS['TL_LANG']['tl_member']['street2'] = array('Adresse (Zusatz)');
$GLOBALS['TL_LANG']['tl_member']['company'] = array('Firma');
$GLOBALS['TL_LANG']['tl_member']['company2'] = array('Firma (Zusatz)');
$GLOBALS['TL_LANG']['tl_member']['phoneCompany'] = array('Telefon (Firma)');
$GLOBALS['TL_LANG']['tl_member']['phone'] = array('Telefon (Direktwahl)');
$GLOBALS['TL_LANG']['tl_member']['gender'] = array('Anrede');
/**
 * Reference
 */

$GLOBALS['TL_LANG']['tl_member']['shippingAddress'] = 'Lieferadresse';



?>