<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */


/**
 * Fields
 */
//$GLOBALS['TL_LANG']['tl_brand'][''] = array('', '');


/**
 * Reference
 */
//$GLOBALS['TL_LANG']['tl_brand'][''] = '';


/**
 * Buttons
 */

$GLOBALS['TL_LANG']['tl_exp_brand']['new']    = array('Nouvelle marque', '');
$GLOBALS['TL_LANG']['tl_exp_brand']['edit']   = array('', '');
$GLOBALS['TL_LANG']['tl_exp_brand']['copy']   = array('', '');
$GLOBALS['TL_LANG']['tl_exp_brand']['delete'] = array('', '');
$GLOBALS['TL_LANG']['tl_exp_brand']['show']   = array('', '');

?>