<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    Language
 * @license    commercial 
 * @filesource
 */

 
/**
 * Miscellaneous 
 */
//$GLOBALS['TL_LANG']['MSC'][''] = '';
$GLOBALS['TL_LANG']['MSC']['maxBrandsOver'] = 'Sie können nicht mehr als 12 Marken einfügen/auswählen';
$GLOBALS['TL_LANG']['MSC']['minBrands'] = 'Sie müssen mindestens 1 Marke auswählen/einfügen';
$GLOBALS['TL_LANG']['MSC']['stand_bill_not_exist'] = array('Der Stand besitzt keine Rechnungsadresse.','Der Stand besitzt keine Rechnungsadresse.');
$GLOBALS['TL_LANG']['MSC']['billing_too_label'] = 'Identisch mit Standverantwortlicher';
$GLOBALS['TL_LANG']['MSC']['nextStep'] = "Nächster Schritt";
$GLOBALS['TL_LANG']['MSC']['prevStep'] = "Zurück";
$GLOBALS['TL_LANG']['MSC']['finalStep'] = "Anmeldung abschliessen";
$GLOBALS['TL_LANG']['MSC']['male'] = 'Herr';
$GLOBALS['TL_LANG']['MSC']['female'] = 'Frau';
$GLOBALS['TL_LANG']['MSC']['male_short'] = 'Herr';
$GLOBALS['TL_LANG']['MSC']['female_short'] = 'Frau';
$GLOBALS['TL_LANG']['MSC']['charMax30_note'] = "max. 30 Char.";
$GLOBALS['TL_LANG']['MSC']['requiredFieldsLabel'] = "* obligatorisches Feld";
$GLOBALS['TL_LANG']['MSC']['standsAlreadySubscribed'] = "Sie sind in unserer Datenbank schon angemeldet. Wenn Sie Ihre Daten ändern wollen, kontaktieren Sie uns bitte per Email an {{email::info@transport-CH.com}}";
$GLOBALS['TL_LANG']['MSC']['textBoxListSuggestionLabel'] = "Erfassen Sie die ersten Zeichen, um Vorschläge zu erhalten";
$GLOBALS['TL_LANG']['MSC']['explainPassword'] = '<strong>Herzlich willkommen</strong> in Ihrem Bereich.<br /> Um ihn definitiv zu schützen, bitten wir Sie, ein neues, persönliches Passwort (mindestens 8 Zeichen) einzugeben.';

$GLOBALS['TL_LANG']['MSC']['subscribeMailConfirm'] = "Anmeldung";

$GLOBALS['TL_LANG']['ERR']['phoneExpotrans'] = "Die Telefonnummer darf nur Zahlen und Leerzeichen enthalten.";
$GLOBALS['TL_LANG']['ERR']['digitMax7'] = "Die Zahl muss zwischen 1 und 7 sein";
$GLOBALS['TL_LANG']['ERR']['digitNo0'] = "Muss grösser als 0 sein";

?>