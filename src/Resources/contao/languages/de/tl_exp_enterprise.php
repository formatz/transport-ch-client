<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */


/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_exp_enterprise']['exhib_name'] = array('Dénomination pour l\'exposition', 'Nom de l\'entreprise pour l\'exposition, qui sera affiché sur le site internet.');

$GLOBALS['TL_LANG']['tl_exp_enterprise']['name'] = array('Société', 'Saisir la société.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['alias'] = array('Alias', 'Alias unique utilisé dans les url à la place de l\'identifiant.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['street'] = array('Rue', 'Saisir le nom et le numéro de la rue.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['postal'] = array('Code postal', 'Saisir le code postal.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['city'] = array('Ville', 'Saisir le nom de la ville.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['country'] = array('Pays', 'Sélectionner le pays.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['phone'] = array('Téléphone', 'Saisir un numéro de téléphone.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['fax'] = array('Fax', 'Saisir un numéro de fax.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['email'] = array('Adresse e-mail', 'Saisir une adresse e-mail valide.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['website'] = array('Site internet', 'Saisir l\'URL d\'un site internet');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['website2'] = array('Site internet 2', 'Saisir l\'URL d\'un site internet');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['language'] = array('Langue', 'Sélectionner la langue.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['groups'] = array('Membre des groupes', 'Affecter l\'utilisateur à un ou plusieurs groupes.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['login'] = array('Permettre la connexion', 'Générer un identifiant et un mot de passe pour le membre.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['username'] = array('Identifiant', 'Saisir un identifiant unique.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['assignDir'] = array('Autoriser un répertoire personnel', 'Affecter un répertoire personnel au membre.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['homeDir'] = array('Répertoire personnel', 'Sélectionner un répertoire personnel pour le membre.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['disable'] = array('Inactif', 'Désactiver temporairement le compte.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['start'] = array('Activé à partir du', 'Activer le compte à partir de ce jour.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['stop'] = array('Désactivé à partir du', 'Désactiver le compte à partir de ce jour.');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['personal_legend'] = 'Données personnelles';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['address_legend'] = 'Adresse';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['contact_legend'] = 'Informations de contact';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['groups_legend'] = 'Groupes';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['login_legend'] = 'Connexion';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['homedir_legend'] = 'Répertoire personnel';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['account_legend'] = 'Paramètres du compte';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['personalData'] = 'Données personnelles';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['addressDetails'] = 'Adresse';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['contactDetails'] = 'Informations de contact';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['loginDetails'] = 'Connexion';
$GLOBALS['TL_LANG']['tl_exp_enterprise']['new'] = array('Nouveau membre', 'Créer un nouveau membre');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['show'] = array('Détails du membre', 'Afficher les détails du membre ID %s');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['edit'] = array('Éditer le membre', 'Éditer le membre ID %s');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['copy'] = array('Dupliquer le membre', 'Dupliquer le membre ID %s');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['delete'] = array('Supprimer le membre', 'Supprimer le membre ID %s');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['toggle'] = array('Activer / désactiver le membre', 'Activer / désactiver le membre ID %s');

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_exp_enterprise'][''] = '';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_exp_enterprise']['new']    = array('Nouvelle entreprise', '');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['edit']   = array('', '');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['copy']   = array('', '');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['delete'] = array('', '');
$GLOBALS['TL_LANG']['tl_exp_enterprise']['show']   = array('', '');

?>