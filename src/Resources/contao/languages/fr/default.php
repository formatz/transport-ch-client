<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    Language
 * @license    commercial 
 * @filesource
 */


/**
 * Miscellaneous
 */
//$GLOBALS['TL_LANG']['MSC'][''] = '';
$GLOBALS['TL_LANG']['MSC']['maxBrandsOver'] = 'Vous ne pouvez sélectionner/ajouter plus de 12 marques';
$GLOBALS['TL_LANG']['MSC']['minBrands'] = 'Vous devez sélectionner/ajouter au moins 1 marque';
$GLOBALS['TL_LANG']['MSC']['stand_bill_not_exist'] = array('Le stand ne comporte pas de compte facturation.','Le stand ne comporte pas de compte facturation.');
$GLOBALS['TL_LANG']['MSC']['billing_too_label'] = 'Identique au/à la Responsable du stand ';
$GLOBALS['TL_LANG']['MSC']['nextStep'] = "Etape suivante";
$GLOBALS['TL_LANG']['MSC']['prevStep'] = "Etape précédente";
$GLOBALS['TL_LANG']['MSC']['finalStep'] = "Finaliser l'inscription";
$GLOBALS['TL_LANG']['MSC']['male'] = 'Monsieur';
$GLOBALS['TL_LANG']['MSC']['female'] = 'Madame';
$GLOBALS['TL_LANG']['MSC']['male_short'] = 'M.';
$GLOBALS['TL_LANG']['MSC']['female_short'] = 'Mme';
$GLOBALS['TL_LANG']['MSC']['charMax30_note'] = "max. 30 car.";
$GLOBALS['TL_LANG']['MSC']['requiredFieldsLabel'] = "* champ obligatoire";
$GLOBALS['TL_LANG']['MSC']['standsAlreadySubscribed'] = "Vous êtes déjà inscrit dans notre base de données. Si vous désirez modifier vos informations, veuillez nous contacter par e-mail à {{email::info@transport-CH.com}}";
$GLOBALS['TL_LANG']['MSC']['textBoxListSuggestionLabel'] = "Tapez les premiers caractères afin d\'obtenir des suggestions";
$GLOBALS['TL_LANG']['MSC']['explainPassword'] = '<strong>Bienvenue</strong> dans votre espace.<br />Afin de le protéger définitivement, nous vous prions d’introduire un <strong>nouveau mot de passe personnel</strong> (minimum 8 caractères).'; 

$GLOBALS['TL_LANG']['MSC']['subscribeMailConfirm'] = "Confirmation d'inscription";

$GLOBALS['TL_LANG']['MSC']['export'] = "Export des données";

$GLOBALS['TL_LANG']['ERR']['phoneExpotrans'] = "Le numéro de téléphone ne doit comporter que des espaces et des chiffres.";
$GLOBALS['TL_LANG']['ERR']['digitMax7'] = "Le nombre doit être compris entre 1 et 7";
$GLOBALS['TL_LANG']['ERR']['digitNo0'] = "Doit être plus grand que 0";

$GLOBALS['TL_LANG']['ERR']['adult18'] = "La personne doit être âgée d'au moins 18 ans.";

?>