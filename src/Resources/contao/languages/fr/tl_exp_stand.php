<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011
 * @author     Format Z - office@format-z.ch
 * @package    ExpotransModule
 * @license    commercial
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_exp_stand'][''] = array('', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['exhib_name'] = array('Nom de l’entreprise tel qu\'il doit figurer dans le catalogue du salon', 'Nom de l’entreprise tel qu’il doit figurer dans le catalogue du salon');

$GLOBALS['TL_LANG']['tl_exp_stand']['name'] = array('Nom interne', 'Saisir le nom du stand interne (temporaire)');
$GLOBALS['TL_LANG']['tl_exp_stand']['alias'] = array('Alias', 'Alias unique utilisé dans les url à la place de l\'identifiant.');
$GLOBALS['TL_LANG']['tl_exp_stand']['company'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_company'] = array('Société', 'Société');
$GLOBALS['TL_LANG']['tl_exp_stand']['company2'] = array('Société (complément)', 'Société (complément)');
$GLOBALS['TL_LANG']['tl_exp_stand']['street'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_street'] = array('Adresse', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['street2'] = array('Adresse (complément)', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['postalCase'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_postalCase'] = array('Case postale', 'Case postale');
$GLOBALS['TL_LANG']['tl_exp_stand']['postal'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_postal'] = array('Code postal', 'Saisir le code postal.');
$GLOBALS['TL_LANG']['tl_exp_stand']['city'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_city'] = array('Ville', 'Saisir le nom de la ville.');
$GLOBALS['TL_LANG']['tl_exp_stand']['country'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_country'] = array('Pays', 'Sélectionner le pays.');
$GLOBALS['TL_LANG']['tl_exp_stand']['phone'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_phone'] = array('Téléphone société', 'Saisir un numéro de téléphone.');
$GLOBALS['TL_LANG']['tl_exp_stand']['fax'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_fax'] = array('Fax', 'Saisir un numéro de fax.');
$GLOBALS['TL_LANG']['tl_exp_stand']['email'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_email'] = array('Adresse e-mail', 'Saisir une adresse e-mail valide.');
$GLOBALS['TL_LANG']['tl_exp_stand']['website'] = $GLOBALS['TL_LANG']['tl_exp_stand']['s_website'] = array('Site internet', 'Saisir l\'URL d\'un site internet');
$GLOBALS['TL_LANG']['tl_exp_stand']['website2'] = array('Site internet 2', 'Saisir l\'URL d\'un site internet');
$GLOBALS['TL_LANG']['tl_exp_stand']['language'] = array('Langue', 'Sélectionner la langue.');
$GLOBALS['TL_LANG']['tl_exp_stand']['categories'] = array('Catégories', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['brands'] = array('Marques représentées', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_opt'] = array('Dimensions optimales', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_min'] = array('Dimensions minimales', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_max'] = array('Dimensions maximales', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_internet'] = array('Connexion internet', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_electric'] = array('Branchement électrique', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_water'] = array('Raccordement d\'eau', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['remark'] = array('Remarques', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['remark_internal'] = array('Remarques internes', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['associations'] = array('Associations', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['assoc_check'] = array('Prix membre accordé', 'Cocher cette case si les affiliations aux associations mentionnées ont été contrôlées et le prix membre accordé.');
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts'] = array('Fronts', '');
$GLOBALS['TL_LANG']['tl_exp_stand']['final_registered'] = array('Inscription définitive','Inscription définitive');
$GLOBALS['TL_LANG']['tl_exp_stand']['date_registered'] = array('Date d\'inscription','Date d\'inscription définitive');
$GLOBALS['TL_LANG']['tl_exp_stand']['was_2009'] = array('Participation en 2009','Participation en 2009');
$GLOBALS['TL_LANG']['tl_exp_stand']['was_2011'] = array('Participation en 2011','Participation en 2011');
//$GLOBALS['TL_LANG']['tl_exp_stand']['exhibition_year'] = array('Année d\'exposition','Année lors de laquelle le stand participe');
$GLOBALS['TL_LANG']['tl_exp_stand']['number'] = array('Numéro de stand','Numéro du stand');
$GLOBALS['TL_LANG']['tl_exp_stand']['out_number'] = array('Numéro de stand extérieur','Numéro du stand extérieur');
$GLOBALS['TL_LANG']['tl_exp_stand']['hall'] = array('Halle','Halle du stand');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_min_h'] = array('Hauteur minimale','Hauteur minimale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_min_l'] = array('Longueur minimale','Longueur minimale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_min_p'] = array('Profondeur minimale','Profondeur minimale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_max_h'] = array('Hauteur maximale','Hauteur maximale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_max_l'] = array('Longueur maximale','Longueur maximale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_max_p'] = array('Profondeur maximale','Profondeur maximale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_opt_h'] = array('Hauteur optimale','Hauteur optimale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_opt_l'] = array('Longueur optimale','Longueur optimale');
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_opt_p'] = array('Profondeur optimale','Profondeur optimale');
$GLOBALS['TL_LANG']['tl_exp_stand']['surface'] = array('Surface attribuée','Surface attribuée');
$GLOBALS['TL_LANG']['tl_exp_stand']['surface_remark'] = array('Description surface','Description surface');
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_attr'] = array('Fronts attribués', 'Fronts attribués');
$GLOBALS['TL_LANG']['tl_exp_stand']['out_sizes_l'] = array('Longueur extérieure','Longueur extérieure');
$GLOBALS['TL_LANG']['tl_exp_stand']['out_sizes_p'] = array('Profondeur extérieure','Profondeur extérieure');
$GLOBALS['TL_LANG']['tl_exp_stand']['surface_final_h'] = array('Hauteur attribuée', 'Hauteur attribuée');
$GLOBALS['TL_LANG']['tl_exp_stand']['out_surface'] = array('Surface extérieure attribuée', 'Surface extérieure attribuée');
$GLOBALS['TL_LANG']['tl_exp_stand']['out_surface_remark'] = array('Description surface extérieure', 'Description surface extérieure');
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars'] = array('Nous exposerons :','Objets d\'exposition');
$GLOBALS['TL_LANG']['tl_exp_stand']['files'] = array('Fichiers joints', 'Vous avez la possibilité de joindre des fichiers à votre dossier personnel.');

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_exp_stand']['export_brut'] = 'Export brut des données';
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_instDetails'] = 'Installations techniques';
$GLOBALS['TL_LANG']['tl_exp_stand']['enterprise_header'] = 'Entreprise';
$GLOBALS['TL_LANG']['tl_exp_stand']['co-exhibitor'] = 'Co-exposant';
$GLOBALS['TL_LANG']['tl_exp_stand']['exhibCat'] = "Inscription catalogue salon";
$GLOBALS['TL_LANG']['tl_exp_stand']['hasSecondExp'] = "Second exposant (Réglement général art. 3.4.).";
$GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails'] = "Exposant";
$GLOBALS['TL_LANG']['tl_exp_stand']['addressDetails2'] = "Second exposant";
$GLOBALS['TL_LANG']['tl_exp_stand']['categoriesDetails'] = "Catégorie de stand";
$GLOBALS['TL_LANG']['tl_exp_stand']['brandsDetails'] = "Marques représentées officiellement";
$GLOBALS['TL_LANG']['tl_exp_stand']['standDetails'] = "Information sur le stand";
$GLOBALS['TL_LANG']['tl_exp_stand']['associationsDetails'] = "Appartenance à une association professionnelle";
$GLOBALS['TL_LANG']['tl_exp_stand']['confirmHeader'] = "Confirmation";
$GLOBALS['TL_LANG']['tl_exp_stand']['textConfirm'] = 'En cochant cette case, le responsable de stand inscrit ci-dessus confirme avoir lu et approuve les <a href="tl_files/transport/Conditions_part/tCH13_Conditions_de_participation_121009D_BAT_lt.pdf" target="_blank">Conditions de participation</a> de l\'édition concernée du Salon suisse du véhicule utilitaire transportCH.';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableTitle'] = "Dimensions";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesOptTitle'] = "Optimal [m]";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesMinTitle'] = "Minimal [m]";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesMaxTitle'] = "Maximal [m]";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableL'] = "Longueur";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableH'] = "Hauteur";
$GLOBALS['TL_LANG']['tl_exp_stand']['sizesTableP'] = "Profondeur";
$GLOBALS['TL_LANG']['tl_exp_stand']['outSizesDetails'] = "Surface en plein-air";
$GLOBALS['TL_LANG']['tl_exp_stand']['hasAssoc'] = "Nous sommes membre d’aucune association ci-dessous";
$GLOBALS['TL_LANG']['tl_exp_stand']['respStandHeader'] = "Responsable de stand";
$GLOBALS['TL_LANG']['tl_exp_stand']['billingStandHeader'] = "Contact de facturation";
$GLOBALS['TL_LANG']['tl_exp_stand']['voirDelaisEtTarifs'] = "";
$GLOBALS['TL_LANG']['tl_exp_stand']['brandsExpl'] = 'Saisissez la marque. Sélectionnez-la dans la liste ou ajoutez-la puis confirmer avec ENTER.';

$GLOBALS['TL_LANG']['tl_exp_stand']['general_header'] = 'Données générales';
$GLOBALS['TL_LANG']['tl_exp_stand']['address_legend'] = 'Exposant';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_header'] = 'Dimensions attribuées';
$GLOBALS['TL_LANG']['tl_exp_stand']['second_exhib_header'] = 'Second exposant';
$GLOBALS['TL_LANG']['tl_exp_stand']['contact_legend'] = 'Informations de contact';
$GLOBALS['TL_LANG']['tl_exp_stand']['contacts_legend'] = 'Membres';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_min_header'] = 'Dimensions minimales';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_max_header'] = 'Dimensions maximales';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_opt_header'] = 'Dimensions optimales';
$GLOBALS['TL_LANG']['tl_exp_stand']['sizes_out_header'] = 'Dimensions extérieures';
$GLOBALS['TL_LANG']['tl_exp_stand']['brands_header'] = 'Marques représentées';
$GLOBALS['TL_LANG']['tl_exp_stand']['categories_header'] = 'Catégories';
$GLOBALS['TL_LANG']['tl_exp_stand']['tech_inst_header'] = 'Installations techniques';
$GLOBALS['TL_LANG']['tl_exp_stand']['associations_header'] = 'Associations';
$GLOBALS['TL_LANG']['tl_exp_stand']['login_legend'] = 'Logins';
$GLOBALS['TL_LANG']['tl_exp_stand']['files_legend'] = 'Annexes au stand';
$GLOBALS['TL_LANG']['tl_exp_stand']['expoCars_legend'] = $GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars'][1];

$GLOBALS['TL_LANG']['tl_exp_stand']['files_droplabel'] = 'Déposez les fichiers désirés à cet endroit ...';


$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['both'] = 'des véhicules lourds et légers';
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['light'] = 'des véhicules légers';
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['heavy'] = 'des véhicules lourds';
$GLOBALS['TL_LANG']['tl_exp_stand']['exposedCars_ref']['no'] = 'aucun véhicule';

$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['0'] = 'Aucun';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['1'] = '1 front';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['2a'] = '2 fronts (coin)';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['2b'] = '2 fronts (vis-à-vis)';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['3'] = '3 fronts';
$GLOBALS['TL_LANG']['tl_exp_stand']['fronts_legend']['4'] = '4 fronts';


$GLOBALS['TL_LANG']['tl_exp_stand']['surface_int'] = 'Surface int.';
$GLOBALS['TL_LANG']['tl_exp_stand']['surface_ext'] = 'Surface ext.';
$GLOBALS['TL_LANG']['tl_exp_stand']['name_short'] = 'Nom stand';
$GLOBALS['TL_LANG']['tl_exp_stand']['isNew'] = 'Nouveau';

$GLOBALS['TL_LANG']['tl_exp_stand']['total_registered_stands'] = 'À ce jour, %s stands sont déjà inscrits.';

/* expo-manager */
$GLOBALS['TL_LANG']['tl_exp_stand']['category']['a'] = 'Poids lourds';
$GLOBALS['TL_LANG']['tl_exp_stand']['category']['b'] = 'Véhicules utilitaires légers';

?>