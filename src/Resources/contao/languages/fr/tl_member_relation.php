<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_member_relation']['relation_type'] = array('Type de relation', 'Sélectionner le type de relation : stand ou entreprise');
$GLOBALS['TL_LANG']['tl_member_relation']['enterprise'] = array('Entreprise', 'Sélectionner l\'entreprise');
$GLOBALS['TL_LANG']['tl_member_relation']['enterprise_title'] = array('Titre de la personne au sein de l\'entreprise', 'Exemple : "Directeur marketing"');
$GLOBALS['TL_LANG']['tl_member_relation']['enterprise_function'] = array('Fonction dans l\'entreprise', 'Sélectionner le type de fonction dans l\'entreprise');
$GLOBALS['TL_LANG']['tl_member_relation']['stand'] = array('Stands inscrits', 'Sélectionner le stand');
$GLOBALS['TL_LANG']['tl_member_relation']['stand_function'] = array('Fonction pour le stand', 'Sélectionner le type de fonction dans le stand');

/**
 * Reference
 */

$GLOBALS['TL_LANG']['tl_member_relation']['add'] = array('Ajouter une relation');



?>