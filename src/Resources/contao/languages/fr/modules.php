<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011
 * @author     Format Z - office@format-z.ch
 * @package    ExpotransModule
 * @license    commercial
 * @filesource
 */


/**
 * Back end modules
 */

$GLOBALS['TL_LANG']['MOD']['enterprise'] = array('Entreprises', 'Modules permettant d\'éditer les informations des entreprises exposantes.');
$GLOBALS['TL_LANG']['MOD']['stand'] = array('Stands', 'Modules permettant d\'éditer les stands de l\'exposition.');
$GLOBALS['TL_LANG']['MOD']['brands'] = array('Marques', 'Permet l\'édition de la liste des marques.');
$GLOBALS['TL_LANG']['MOD']['categories'] = array('Catégories de stand', 'Permet l\'édition de la liste des catégories de l\'exposition.');
$GLOBALS['TL_LANG']['MOD']['productCategories'] = array('Catégories de produit', 'Permet l\'édition de la liste des catégories de l\'exposition.');
$GLOBALS['TL_LANG']['MOD']['association'] = array('Associations', 'Permet l\'édition de la liste des associations disponibles.');

$GLOBALS['TL_LANG']['MOD']['registered'] = array('Inscriptions 2013', 'Tableau récapitulatif des inscription au salon 2013');

/*
$GLOBALS['TL_LANG']['MOD']['expotrans']['contact_types'] = array(
	0 => 'Correspondance',
	1 => 'Facturation',
	2 => 'Directeur',
	3 => 'Autre'
);
*/

/**
 * Front end modules
 */
//$GLOBALS['TL_LANG']['FMD'][''] = array('', '');
$GLOBALS['TL_LANG']['FMD']['stand_resp_data'] = array('Editer le responsable de stand', 'Permet l\'édition des données du responsable de stand');
$GLOBALS['TL_LANG']['FMD']['stand_bill_data'] = array('Editer le contact facturation', 'Permet l\'édition des données du contact facturation');
$GLOBALS['TL_LANG']['FMD']['stand_edit'] = array('Edition des données du stand', 'Permet l\'édition des données de stand');
$GLOBALS['TL_LANG']['FMD']['stand_edit2'] = array('Edition des données du stand 2', 'Permet l\'édition des données de stand');
$GLOBALS['TL_LANG']['FMD']['stand_resume'] = array('Résumé des données d\'inscription', 'Affiche les données entrées lors de l\'inscription');
$GLOBALS['TL_LANG']['FMD']['password_lost_expotrans'] = array('Mot de passe perdu (expotrans)', 'Crée un formulaire de demande de nouveau mot de passe. Spécial expotrans.');
$GLOBALS['TL_LANG']['FMD']['stand_search'] = array('Catalogue exposants', 'Affiche sous forme de module de recherche le catalogue exposant');

$GLOBALS['TL_LANG']['FMD']['stand_registered_total'] = array('Compteur des stands inscrits', 'Affiche le total des stands inscrits à ce jour, à partir de 100 inscriptions');

?>
