<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_member']['hasEnterprise'] = array('Appartient à une entreprise.','');
$GLOBALS['TL_LANG']['tl_member']['enterprises'] = array('Entreprises', 'Sélectionner les entreprises de l\'utilisateur et assignez lui un rôle particulier');
$GLOBALS['TL_LANG']['tl_member']['generateAccount'] = array('Générer un accompte', 'Cochez la case afin de générer un nom d\'utilisateur et un mot de passe qui sera envoyé automatiquement à l\'utilisateur (Vérifiez l\'adresse email).');
$GLOBALS['TL_LANG']['tl_member']['access_type'] = array('Type d\'accès', 'Est-ce une personne (par défaut), ou uniquement un accès pour un stand (login / mot de passe).');
$GLOBALS['TL_LANG']['tl_member']['stand'] = array('Stand', 'Sélectionner le stand en relation avec cet accès.');
$GLOBALS['TL_LANG']['tl_member']['postalCase'] = array('Case postale', 'Case postale');
$GLOBALS['TL_LANG']['tl_member']['street'] = array('Adresse');
$GLOBALS['TL_LANG']['tl_member']['street2'] = array('Adresse (complément)');
$GLOBALS['TL_LANG']['tl_member']['company'] = array('Société');
$GLOBALS['TL_LANG']['tl_member']['company2'] = array('Société (complément)');
$GLOBALS['TL_LANG']['tl_member']['phoneCompany'] = array('Téléphone (société)');
$GLOBALS['TL_LANG']['tl_member']['phone'] = array('Téléphone (ligne directe)');
$GLOBALS['TL_LANG']['tl_member']['gender'] = array('Titre');
/**
 * Reference
 */

$GLOBALS['TL_LANG']['tl_member']['shippingAddress'] = 'Adresse de livraison';



?>