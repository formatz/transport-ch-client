<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Expotrans SA 2011 
 * @author     Format Z - office@format-z.ch 
 * @package    ExpotransModule 
 * @license    commercial 
 * @filesource
 */

/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_exp_stand_co']['type'] = array('Type d\'exposant', 'Choisir le type d\'exposant');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['firstname'] = array('Prénom', 'Prénom de la personne de contact');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['lastname']  = array('Nom', 'Nom de la personne de contact');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['c_email']   = array('Email', 'Adresse email de la personne de contact');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['c_phone']   = array('Téléphone', 'Numéro de téléphone de la personne de contact');

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_exp_stand_co']['type_ref']['co'] = 'Co-exposant';
$GLOBALS['TL_LANG']['tl_exp_stand_co']['type_ref']['second'] = 'Second exposant';

$GLOBALS['TL_LANG']['tl_exp_stand_co']['general_header'] = 'Données générales';
$GLOBALS['TL_LANG']['tl_exp_stand_co']['address_legend'] = 'Exposant';
$GLOBALS['TL_LANG']['tl_exp_stand_co']['contact_legend'] = 'Informations de contact';
$GLOBALS['TL_LANG']['tl_exp_stand_co']['contact_person_legend'] = 'Personne de contact';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_exp_stand_co']['new']    = array('Nouveau second exposant/co-exposant', 'Nouveau second exposant/co-exposant');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['edit']   = array('Editer le stand', '');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['copy']   = array('Dupliquer le stand', '');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['delete'] = array('Supprimer le stand', '');
$GLOBALS['TL_LANG']['tl_exp_stand_co']['show']   = array('Afficher les détails', '');

?>