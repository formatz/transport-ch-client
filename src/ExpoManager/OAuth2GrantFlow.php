<?php
/**
 * Created by PhpStorm.
 * User: fabien
 * Date: 18.09.15
 * Time: 15:53.
 */

namespace Formatz\ExpomanagerClientBundle\ExpoManager;

use Contao\Request;
use Symfony\Component\Filesystem\Filesystem;

class OAuth2GrantFlow
{
    /** @var Filesystem */
    protected $fileSystem;
    /** @var string */
    protected $cacheDir;

    /**
     * @var string|null
     */
    private $client_id;

    /**
     * @var string|null
     */
    private $client_secret;

    /**
     * @var string|null
     */
    private $username;

    /**
     * @var string|null
     */
    private $password;

    /**
     * @var string|null
     */
    protected $access_token = null;

    /**
     * @var \Datetime|null
     */
    protected $tokenExpiration = null;

    /**
     * @var string|null
     */
    protected $tokenUrl = '/oauth/v2/token';

    /**
     * @var string|null
     */
    protected $domainUrl = null;

    /**
     * OAuth2GrantFlow constructor.
     *
     * @param string $cacheDir
     */
    public function __construct(Filesystem $fileSystem, $cacheDir)
    {
        $this->fileSystem = $fileSystem;
        $this->cacheDir = $cacheDir;
    }

    /**
     * @param string|null $client_id
     */
    public function setClientId($client_id)
    {
        $this->client_id = $client_id;

        return $this;
    }

    /**
     * @param string|null $client_secret
     */
    public function setClientSecret($client_secret)
    {
        $this->client_secret = $client_secret;

        return $this;
    }

    /**
     * @param string|null $password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string|null $username
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string|null $tokenUrl
     */
    public function setTokenUrl($tokenUrl)
    {
        $this->tokenUrl = $tokenUrl;

        return $this;
    }

    /**
     * @param string|null $domainUrl
     */
    public function setDomainUrl($domainUrl)
    {
        $this->domainUrl = $domainUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    protected function getToken()
    {
        if (null === $this->access_token) {
            $this->requestAccessToken();
        }

        return $this->access_token;
    }

    /**
     * @throws \Exception
     */
    private function requestAccessToken()
    {
        if (null === $this->access_token) {
            $cacheFile = $this->cacheDir.'/oauth_token.json';
            if ($this->fileSystem->exists($cacheFile)) {
                $requestContent = file_get_contents($cacheFile);
                $responseData = json_decode($requestContent);
                $this->tokenExpiration = new \DateTime($responseData->expires_at);
            } else {
                $needNewToken = true;
            }

            if ($this->tokenExpiration <= new \DateTime('-5 seconds')) {
                //token expired
                $needNewToken = true;
            }

            if (true === $needNewToken) {
                if (!empty($this->username) && !empty($this->password)) {
                    $postParams = [
                        'grant_type' => 'password',
                        'client_id' => $this->client_id,
                        'client_secret' => $this->client_secret,
                        'username' => $this->username,
                        'password' => $this->password,
                    ];
                } else {
                    $postParams = [
                        'grant_type' => 'client_credentials',
                        'client_id' => $this->client_id,
                        'client_secret' => $this->client_secret,
                    ];
                }
                $data = [];
                foreach ($postParams as $key => $value) {
                    $data[] = $key.'='.$value;
                }
                $request = new Request();
                $request->setHeader('Accept', '*/*');
                $request->setHeader('Content-Type', 'application/x-www-form-urlencoded');
                $request->send($this->domainUrl.$this->tokenUrl, implode('&', $data), 'POST');
                if (!$request->hasError()) {
                    $response = $request->response;
                    $responseData = json_decode($response);
                    $expiration = new \DateTime();
                    $expiration->add(new \DateInterval('PT'.$responseData->expires_in.'S'));
                    $responseData->expires_at = $expiration->format(\DateTime::ATOM);
                    $this->tokenExpiration = $expiration;

                    file_put_contents($cacheFile, json_encode($responseData));
                } else {
                    throw new \Exception('Cannot query a request token. "'.$request->error.'"');
                }
            }

            $this->access_token = $responseData->access_token;
        }

        return $this->access_token;
    }

    /**
     * @param $requestUrl string Url suffix to append to domainUrl
     * @param null   $data
     * @param string $method
     * @param string $contentType
     *
     * @return mixed|null
     */
    public function request($requestUrl, $data = null, $method = 'GET', $contentType = 'application/json')
    {
        $request = new Request();
        $request->setHeader('Authorization', 'Bearer '.$this->getToken());
        $request->setHeader('Accept', $contentType);

        if($data === null && $method === 'GET') {
            $useCache = true;
            $cacheFile = $this->cacheDir.'/' . str_replace('/', '_', $requestUrl) . '_' . date('YmdH') . '.json';
            if(file_exists($cacheFile)) {
                $content = file_get_contents($cacheFile);
                return json_decode($content);
            }
        }

        $request->send($this->domainUrl.$requestUrl, $data, $method);

        if ('application/json' == $contentType) {
            if($useCache) {
                file_put_contents($cacheFile, $request->response);
            }
            return json_decode($request->response);
        }

        return $request->response;
    }
}
