<?php

namespace Formatz\ExpomanagerClientBundle\ExpoManager;

class ApiClient
{
    /** @var OAuth2GrantFlow */
    protected $apiGateway;

    /**
     * ApiClient constructor.
     */
    public function __construct(OAuth2GrantFlow $auth2GrantFlow)
    {
        $this->apiGateway = $auth2GrantFlow;
    }

    public function search($keywords)
    {
        return $this->apiGateway->request('/api/search/'.rawurlencode($keywords));
    }

    public function getStands()
    {
        return $this->apiGateway->request('/api/stands/');
    }

    public function getStand($id)
    {
        return $this->apiGateway->request('/api/stands/'.$id);
    }

    public function getExhibitors($withCo = false)
    {
        if ($withCo) {
            return $this->apiGateway->request('/api/exhibitors/and-co');
        }

        return $this->apiGateway->request('/api/exhibitors/');
    }

    public function getExhibitor($id)
    {
        return $this->apiGateway->request('/api/exhibitors/'.$id);
    }

    public function getBrands()
    {
        return $this->apiGateway->request('/api/brands/');
    }

    public function getProductCategories()
    {
        return $this->apiGateway->request('/api/product-categories/');
    }

    public function getCategories()
    {
        $categories = <<<EOT
  {
	"data":
	[
		{
			"id": 2,
			"letter": "a",
			"nameFr": "Constructeur/importateur officiel poids lourds",
			"nameDe": "Importeure/Vertretungen schwerer Nutzfahrzeuge"
		},
		{
			"id": 3,
			"letter": "b",
			"nameFr": "Constructeur/importateur officiel VUL (véhicules utilitaires légers)",
			"nameDe": "Importeure/Vertretungen leichter Nutzfahrzeuge"
		},
		{
			"id": 4,
			"letter": "c",
			"nameFr": "Constructeur de véhicules, carrosseries, remorques et engins de manutention et de levage",
			"nameDe": "Anhänger-, Fahrzeug-, Karosserie- und Hebegerätebauer"
		},
		{
			"id": 5,
			"letter": "d",
			"nameFr": "Fournisseur d'équipements, de composants et d’accessoires pour véhicules utilitaires",
			"nameDe": "Ausstattungs-, Ausrüstungs- und Zubehöranbieter für Nutzfahrzeuge"
		},
		{
			"id": 6,
			"letter": "f",
			"nameFr": "Prestataire de service",
			"nameDe": "Dienstleistungsanbieter"
		},
		{
			"id": 8,
			"letter": "g",
			"nameFr": "Formation de base et continue",
			"nameDe": "Aus- und Weiterbildung"
		},
		{
			"id": 9,
			"letter": "e",
			"nameFr": "Fournisseur d'équipements et d’outillage pour garage",
			"nameDe": "Ausrüstungs- und Werkzeuganbieter für Garagen"
		},
		{
			"id": 10,
			"letter": "h",
			"nameFr": "Associations",
			"nameDe": "Fachverbände"
		}
	]
}
EOT;

        return json_decode($categories)->data;
    }
}
