<?php
/**
 * Created by PhpStorm.
 * User: cedricbapst
 * Date: 05.09.18
 * Time: 16:41.
 */

namespace Formatz\ExpomanagerClientBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Formatz\ExpomanagerClientBundle\FormatzExpomanagerClientBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(FormatzExpomanagerClientBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
